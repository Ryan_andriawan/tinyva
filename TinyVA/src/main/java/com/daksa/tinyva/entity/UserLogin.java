package com.daksa.tinyva.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="userlogin")
public class UserLogin implements Serializable{
    
     private static final long serialVersionUID = 1L;
     @Id
     @Column(name="username")
     @NotNull
     private String username;
     @Column(name="password")
     @NotNull
     @Size(min = 4)
     private String password;
     @Column(name="name")
     @NotNull
     private String name;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
