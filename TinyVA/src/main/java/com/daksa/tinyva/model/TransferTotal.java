
package com.daksa.tinyva.model;

import java.math.BigDecimal;
import java.util.Date;


public class TransferTotal {
    
    
    private BigDecimal amount;
    private Date date;
    
    public TransferTotal(Date date,BigDecimal amount){
        
        this.date = date;
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
