
package com.daksa.tinyva.service;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.exception.InvalidUserException;
import com.daksa.tinyva.repository.UserRepository;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
public class UserService implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private UserRepository repo;
    @Inject
    private EntityManager em;
    
    public void create(UserLogin user)throws InvalidUserException{
        
        UserLogin check = repo.findUser(user.getUsername());
        if(check == null){
            em.persist(user);
        }
        else
            throw new InvalidUserException("Id Sudah Digunakan");
    }
    
    public void update(UserLogin user){
        em.merge(user);
    }
    
}
