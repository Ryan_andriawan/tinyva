package com.daksa.tinyva.service;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InvalidAccountException;
import com.daksa.tinyva.repository.AccountRepository;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless

public class AccountService implements Serializable{
    
    @Inject
    private EntityManager em;
    
    @Inject
    private AccountRepository repo;
    
    public AccountService(){
        
    }
    
    public void create(Account account)throws InvalidAccountException{
        Account acc = repo.findAccount(account.getId());
        if(acc== null){
        em.persist(account);
        }
        else{
            throw new InvalidAccountException("Id sudah digunakan");
        }
    }
    
    public void update(Account account){
        
            em.merge(account);
    }
    
    public void afterTransfer(Account sender,Account receiver){
        
            em.merge(sender);
            em.merge(receiver);
    }
    
    
}