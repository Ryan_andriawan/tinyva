
package com.daksa.tinyva.service;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.entity.TransferHistory;
import com.daksa.tinyva.exception.InsufficientBalanceException;
import com.daksa.tinyva.exception.InvalidTransferException;
import com.daksa.tinyva.model.TransferTotal;
import com.daksa.tinyva.repository.TransferRepository;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Stateless
public class TransferService {
    
    @Inject
    private EntityManager em;
    
    @Inject
    private TransferRepository repo;
    
    @Inject
    private AccountService accountService;
    
    
    public void create(Account sender,Account receiver,BigDecimal ammount)throws InsufficientBalanceException,InvalidTransferException{
        
        String newID = lastId();
        if(sender.getId().equals(receiver.getId()) == true){
            throw new InvalidTransferException("Pengirim dan Penerima tidak boleh sama");
        }
        else
        {
            if(sender.getBalance().compareTo(ammount) == -1){
                if(sender.getAllowNegative() == false){
                    throw new InsufficientBalanceException("saldo "+sender.getName()+" tidak cukup untuk melakukan transfer");
                    
                }
                else{
                    TransferHistory transfer = new TransferHistory();
                    transfer.setId(newID);
                    transfer.setAmmount(ammount);
                    transfer.setIdAccount(sender);
                    transfer.setDestination(receiver);
                    transfer.setDate(new Date());
                    transfer.setChartDate(new Date());
                    
                    sender.setBalance(sender.getBalance().subtract(ammount));
                    receiver.setBalance(receiver.getBalance().add(ammount));
                                       
                    
                    
                    
                    em.persist(transfer);
                }
            }
            else{
                    TransferHistory transfer = new TransferHistory();
                    transfer.setId(newID);
                    transfer.setAmmount(ammount);
                    transfer.setIdAccount(sender);
                    transfer.setDestination(receiver);
                    transfer.setDate(new Date());
                    transfer.setChartDate(new Date());
                    
                    sender.setBalance(sender.getBalance().subtract(ammount));
                    receiver.setBalance(receiver.getBalance().add(ammount));
                    
                    
                    em.persist(transfer);
            }
        } 
    }
    
    
    private String lastId(){
        Date tgl = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(tgl);
        Integer year = cal.get(Calendar.YEAR);
        String LastId= repo.getLastID(year);
        Integer no;
        
        if(LastId == null || LastId.isEmpty()){
          no=1;
        }else{
          no = Integer.parseInt(LastId.substring(7, 10));
          no++;  
        }
        String nomor = "000"+no.toString();
        String newID = "trf"+year.toString()+nomor.substring(nomor.length()-3);
        
        return  newID;
    }
}
