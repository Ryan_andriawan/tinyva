
package com.daksa.tinyva.repository;

import com.daksa.tinyva.entity.UserLogin;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@Dependent
public class UserRepository implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Inject
    private EntityManager em;
    
    public UserLogin findUser(String id){
        
        return em.find(UserLogin.class, id);
    }
    
    
}
