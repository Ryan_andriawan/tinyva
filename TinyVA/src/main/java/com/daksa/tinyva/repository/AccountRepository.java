package com.daksa.tinyva.repository;

import com.daksa.tinyva.entity.Account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.primefaces.model.SortOrder;

@Dependent
public class AccountRepository implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private EntityManager em;
    
    
    public Account findAccount(String id){
        return em.find(Account.class, id);
    }
    
    public List<Account> getAllAccount(){
        
            TypedQuery<Account> query = em.createNamedQuery("Account.findAll", Account.class);
            return query.getResultList();
        
    }
    
    public List<Account> getLazyAllAccount(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters){
        
        String querySelect = "SELECT e FROM Account e ORDER BY e.id";
        
            if(sortField!=null && !sortField.isEmpty() && sortOrder != null){
                if(sortOrder.equals(SortOrder.ASCENDING)){
                     querySelect = "SELECT e FROM Account e ORDER BY e."+sortField+" ASC";
                }
                else{
                     querySelect = "SELECT e FROM Account e ORDER BY e."+sortField+" DESC";
                }
                
            }else{
                  querySelect = "SELECT e FROM Account e ORDER BY e.id";
            }
            
            if(filters != null && !filters.isEmpty()){
                String key = "";
                List<String> keys = new ArrayList<>(filters.keySet());
                for(String a:keys){
                    key = a;
                }
                querySelect = "SELECT e FROM Account e WHERE e."+key+" LIKE '%"+filters.get(key)+"%'";
                
                if(keys.size()>1){
                    for (int i =0;i<keys.size()-1;i++) {
                        key = keys.get(i);
                        querySelect = querySelect+ "AND e."+key+" LIKE '%"+filters.get(key)+"%'";
                    }
                }
                
                if(sortField!=null && !sortField.isEmpty() && sortOrder != null){
                    if(sortOrder.equals(SortOrder.ASCENDING)){
                         querySelect = querySelect+ " ORDER BY e."+sortField+" ASC";
                    }
                    else{
                         querySelect = querySelect+ " ORDER BY e."+sortField+" DESC";
                    }

                }else{
                      querySelect = querySelect+" ORDER by e."+key;
                }
                
            }
            Query query = em.createQuery(querySelect,Account.class);
            query.setFirstResult(first);
            query.setMaxResults(pageSize);
            return query.getResultList();
            
    }
}
