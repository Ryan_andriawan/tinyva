
package com.daksa.tinyva.repository;

import com.daksa.tinyva.entity.TransferHistory;
import com.daksa.tinyva.model.TransferTotal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.primefaces.model.SortOrder;

@Dependent
public class TransferRepository implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private EntityManager em;
    
    
    public TransferHistory findTransferHistory(String id){
        return em.find(TransferHistory.class, id);
    }
    
    public List<TransferHistory> getAllTransferHistory(){
            TypedQuery<TransferHistory> query = em.createNamedQuery("TransferHistory.findAll", TransferHistory.class);          
            return query.getResultList();
    }
    
    public List<Date> getChartDate(){
        
            TypedQuery<Date> query = em.createNamedQuery("TransferHistory.chartDate", Date.class); 
            return query.getResultList();
    }
    
    public List<TransferTotal> listTotalPerDate  (){
        
         String querySelect = "SELECT NEW com.daksa.tinyva.model.TransferTotal(e.chartDate,SUM(e.ammount)) FROM TransferHistory e GROUP BY e.chartDate";
         TypedQuery<TransferTotal> query = em.createQuery(querySelect,TransferTotal.class);
         return  query.getResultList();
    }
    
    public String getLastID(Integer year){
       
            String querySelect = "SELECT MAX(e) FROM TransferHistory e WHERE SUBSTRING(e.id,4,4) = '"+year+"'";
            TypedQuery<TransferHistory> query = em.createQuery(querySelect,TransferHistory.class);
            TransferHistory history = query.getSingleResult();
            if(history == null){
                return null;
            }else
            return history.getId();
    }
    
    public List<TransferHistory> getLazyAllTransfer(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters){
        
        String querySelect;
        
        
            if(sortField!=null && !sortField.isEmpty() && sortOrder != null){
                if(sortOrder.equals(SortOrder.ASCENDING)){
                     querySelect = "SELECT e FROM TransferHistory e ORDER BY e."+sortField+" ASC";
                }
                else{
                     querySelect = "SELECT e FROM TransferHistory e ORDER BY e."+sortField+" DESC";
                }
                
            }else{
                  querySelect = "SELECT e FROM TransferHistory e ORDER BY e.id";
            }
            
            if(filters != null && !filters.isEmpty()){
                String key = "";
                List<String> keys = new ArrayList<>(filters.keySet());
                
                for(String a:keys){
                    key = a;
                }
                querySelect = "SELECT e FROM TransferHistory e WHERE e."+key+" LIKE '%"+filters.get(key)+"%'";
                
                if(keys.size()>1){
                    for (int i =0;i<keys.size()-1;i++) {
                        key = keys.get(i);
                        querySelect = querySelect+ "AND e."+key+" LIKE '%"+filters.get(key)+"%'";
                    }
                }
                
                if(sortField!=null && !sortField.isEmpty() && sortOrder != null){
                    if(sortOrder.equals(SortOrder.ASCENDING)){
                         querySelect = querySelect+" ORDER BY e."+sortField+" ASC";
                    }
                    else{
                         querySelect =  querySelect+" ORDER BY e."+sortField+" DESC";
                    }

                }else{
                      querySelect = querySelect+" ORDER by e."+key;
                }
                
            }
            Query query = em.createQuery(querySelect,TransferHistory.class);
            query.setFirstResult(first);
            query.setMaxResults(pageSize);
            return query.getResultList();
    }
    
    public Long countTransfer(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters){
        
        String querySelect = "SELECT COUNT(e) FROM TransferHistory e";
        
            if(filters != null && !filters.isEmpty()){
                String key = "";
                List<String> keys = new ArrayList<>(filters.keySet());
                
                for(String a:keys){
                    key = a;
                }
                querySelect = "SELECT COUNT(e) FROM TransferHistory e WHERE e."+key+" LIKE '%"+filters.get(key)+"%'";
                
                if(keys.size()>1){
                    for (int i =0;i<keys.size()-1;i++) {
                        key = keys.get(i);
                        querySelect = querySelect+ "AND e."+key+" LIKE '%"+filters.get(key)+"%'";
                    }
                }
            }
            TypedQuery<Long> query = em.createQuery(querySelect,Long.class);
            return query.getSingleResult();
            
    }
}
