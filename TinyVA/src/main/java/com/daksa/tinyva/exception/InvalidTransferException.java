
package com.daksa.tinyva.exception;


public class InvalidTransferException extends Exception{
    
    public InvalidTransferException(){
        
    }
    
    public InvalidTransferException(String msg){
        super(msg);
    }
}
