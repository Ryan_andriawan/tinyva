package com.daksa.tinyva.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class InvalidAccountException extends Exception{
    
    public InvalidAccountException(){
        
    }
    
    public InvalidAccountException(String msg){
        
        super(msg);
    }
}
