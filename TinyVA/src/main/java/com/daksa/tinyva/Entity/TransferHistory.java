/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tinyva.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.persistence.Table;


@Entity
@Table(name = "transfer")
@NamedQueries({
    @NamedQuery(name="TransferHistory.findAll",query="SELECT e FROM TransferHistory e ORDER BY e.id"),
    /*@NamedQuery(name="TransferHistory.findAllFilter",query="SELECT e FROM TransferHistory e WHERE e.:tes = :tes2 ORDER BY e.id"),*/
    @NamedQuery(name="TransferHistory.chartDate",query="SELECT DISTINCT(e.chartDate) FROM TransferHistory e ORDER BY e.chartDate ")
})

public class TransferHistory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="id")
    private String id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date date;
    @Column(name = "chart_date")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date chartDate;
    @Column(name = "ammount",scale = 2,precision = 19)
    @NotNull
    private BigDecimal ammount;
    @JoinColumn(name ="account_id",referencedColumnName = "id")
    @NotNull
    @ManyToOne(optional = false)
    private Account idAccount;
    @JoinColumn(name ="account_destination",referencedColumnName = "id")
    @NotNull
    @ManyToOne(optional = false)
    private Account destination;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public Account getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Account idAccount) {
        this.idAccount = idAccount;
    }

    public Account getDestination() {
        return destination;
    }

    public void setDestination(Account destination) {
        this.destination = destination;
    }

    public Date getChartDate() {
        return chartDate;
    }

    public void setChartDate(Date chartDate) {
        this.chartDate = chartDate;
    }
    
}
