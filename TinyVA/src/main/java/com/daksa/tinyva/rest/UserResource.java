
package com.daksa.tinyva.rest;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.exception.InvalidUserException;
import com.daksa.tinyva.repository.UserRepository;
import com.daksa.tinyva.service.UserService;
import javax.ws.rs.Path;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("user")
public class UserResource {
    
    @Inject
    private UserRepository repo;
    @Inject
    private UserService service;
    
    
    @GET
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserLogin getAllUser(@PathParam("param") String id){
        
        return repo.findUser(id);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(UserLogin user){
        try {
            service.create(user);
            String result = "created: user - "+user.getUsername();
            return Response.status(201).entity(result).build();
        } catch (InvalidUserException ex) {
            
            return Response.status(400).entity(ex.getMessage()).build();
        }
    }
    
    @PUT
    @Path("/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("param") String id,UserLogin user){
        
        service.update(user);
        String result = "update: user - "+user.getUsername();
        return Response.status(202).entity(result).build();
    }

}
