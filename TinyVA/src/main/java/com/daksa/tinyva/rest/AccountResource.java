package com.daksa.tinyva.rest;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InvalidAccountException;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.service.AccountService;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("account")
public class AccountResource {
    
    @Inject
    private AccountRepository repo;
    @Inject
    private AccountService service;
    
    @GET
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account accountById(@PathParam("param") String id){
        
        Account acc = new Account();
        acc = repo.findAccount(id);
        return acc;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Account> getAllAccount(){
        
        List<Account> accs ;
        accs = repo.getAllAccount();
        return accs;
    }
    
//    JSON: 
//    {
//        "id": "001",
//        "name": "Joni",
//        "dob": "1995-02-08",
//        "allowNegative": true,
//        "balance":0.00
//    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(Account acc){
        
        try {
            service.create(acc);
            String result = "created: id: "+ acc.getId()+" ";
            return Response.status(201).entity(result).build();
            
        } catch (InvalidAccountException ex) {
            System.out.println(ex.getMessage());
            return Response.status(400).entity(ex.getMessage()).build();
        }
    }
    
    @PUT
    @Path("/{param}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAccount(@PathParam("param") String id, Account acc){
        service.update(acc);
        String result = "update: "+ acc;
        return Response.status(201).entity(result).build();
    }
}
