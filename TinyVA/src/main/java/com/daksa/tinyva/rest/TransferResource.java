
package com.daksa.tinyva.rest;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.entity.TransferHistory;
import com.daksa.tinyva.exception.InsufficientBalanceException;
import com.daksa.tinyva.exception.InvalidTransferException;
import com.daksa.tinyva.model.Transfer;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.repository.TransferRepository;
import com.daksa.tinyva.service.TransferService;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("transfer")
public class TransferResource {
    
    @Inject
    private TransferRepository repo;
    
    @Inject
    private AccountRepository accountRepo;
    
    @Inject
    private TransferService service;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TransferHistory> getAllTransfer(){
        
       
        return repo.getAllTransferHistory();
    }
    
    @GET
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public TransferHistory getTransferById(@PathParam("param") String id){
        
        TransferHistory trans = new TransferHistory();
        trans = repo.findTransferHistory(id);
        return trans;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTransfer(Transfer trans){
        
        Account sender = accountRepo.findAccount(trans.getSender());
        Account receiver = accountRepo.findAccount(trans.getReceiver());
        
        try {
            service.create(sender, receiver, trans.getAmount());
            return Response.status(201).build();
        } catch (InsufficientBalanceException | InvalidTransferException ex) {
            return Response.status(400).entity(ex.getMessage()).build();
        }
        
    }
}
