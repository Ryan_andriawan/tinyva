
package com.daksa.tinyva.web;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.inject.Inject;

@Named
@SessionScoped
public class SessionBacking implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Inject
    private FacesContext facesContext;
    
    private String username;
    private String name;

    public String getUsername() {
        
        return username;
    }

    public void setUsername(String Username) {
        System.out.println(Username);
        this.username = Username;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }
    
    public String logout(){
        
        facesContext.getExternalContext().invalidateSession();
        return "/login.xhtml?faces-redirect=true";
    }
    
}
