package com.daksa.tinyva.web;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InvalidAccountException;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.service.AccountService;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Inject;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;


@Named
@ViewScoped
public class AccountBacking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private AccountService service;
    
    @Inject
    private AccountRepository repo;
    
    @Inject
    private FacesContext facesContext;
    
    private LazyDataModel<Account> lazyAccountModel;
    private String id;
    private String name;
    private Date dob;
    private Boolean allowNegative;
    private Account account; 
    
    @PostConstruct
    public void init(){
        lazyAccountModel = new LazyDataModel<Account>() {
            @Override
            public List<Account> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List<Account> data;
                List<Account> allData;
                data = repo.getLazyAllAccount(first, pageSize, sortField, sortOrder, filters);
                allData = repo.getAllAccount();
                if(filters != null && !filters.isEmpty()){
                    this.setRowCount(data.size());
                }else{
                    this.setRowCount(allData.size());
                }
                return data;
            } 
        };
    }
    
    public Date getDate(){
        return new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Boolean getAllowNegative() {
        return allowNegative;
    }

    public void setAllowNegative(Boolean allowNegative) {
        this.allowNegative = allowNegative;
    }

    public Account getAccount() {
        return account;
    }

    public LazyDataModel<Account> getLazyAccountModel(){
        return lazyAccountModel;
    }

    public void create(){
        
        account = new Account();
        Boolean cek = true;
        if(id == null || id.isEmpty() ){
            addMessage("Id tidak boleh kosong",false);
            cek = false;
        }
        if(name == null || name.isEmpty() ){
            addMessage("Nama tidak boleh kosong",false);
            cek = false;
        }
        if(dob == null){
            addMessage("Tanggal Lahir tidak boleh kosong",false);
            cek = false;
        }
        if(cek == true){
            account.setId(id);
            account.setName(name);
            account.setDob(dob);
            account.setAllowNegative(allowNegative);
            account.setBalance(new BigDecimal(0));
            try {
                service.create(account); 

                addMessage("Berhasil",true);
                
                
            } catch (InvalidAccountException ex) {
                addMessage(ex.getMessage(),false);
                
            }
            
            id= null;
            name = null;
            dob = null;
            allowNegative = false;
            
        }
    }
    
    private void addMessage(String message,boolean cek) {
        String summary = message;
        if(cek == true){
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, "Success"));
        }else{
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, "Failed"));
        }
    }
    
  
}
