package com.daksa.tinyva.web;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.repository.UserRepository;
import com.daksa.tinyva.service.UserService;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class LoginBacking implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Inject
    private UserService service;
    
    @Inject
    private UserRepository repo;
    
    @Inject
    private SessionBacking session;
    
    @Inject
    private FacesContext facesContext;
    
    private String username;
    private String password;
    private String name;
    private String newPassword;
    private String oldPassword;
    private String confirmPassword;

    
    private UserLogin user;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public UserLogin getUser() {
        return user;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    
    public SessionBacking getSession() {
        return session;
    }
    
    public void update(){
        Boolean cek = false;
        user = repo.findUser(session.getUsername());
        if(!user.getName().equals(name) &&(name!=null && !name.isEmpty())){
            user.setName(name);
            session.setName(name);
            cek = true;
        }
        if(!oldPassword.isEmpty()){
            if(newPassword.isEmpty() && confirmPassword.isEmpty()){
                addMessage("Password baru tidak boleh kosong",false);
                reset();
            }
            else{
                if(!oldPassword.equals(user.getPassword())){
                    addMessage("Password salah",false);
                    reset();
                }
                else{
                    if(newPassword.equals(confirmPassword)){
                        user.setPassword(newPassword);
                        cek = true;
                    }
                    else{
                         addMessage("Konfirmasi password tidak sama",false);
                         reset();
                    }
                }
            }
        }
        if(cek == true){
            service.update(user);
            reset();
            addMessage("Updated",true);
        }
        
    }
    
    public void reset(){
        name = null;
        oldPassword = null;
        newPassword = null;
        confirmPassword = null;
    }
    
    public void login(){
        if( username == null || username.isEmpty() ){
            addMessage("Username tidak boleh kosong",false);
        }
        if(password == null || password.isEmpty() ){
            addMessage("Password tidak boleh kosong",false);
        }
        else{
            user = repo.findUser(username);
            if(user == null){
                addMessage("Username tidak terdaftar",false);
            }
            else{
                if(password.equals(user.getPassword())){
                    session.setUsername(username);
                    session.setName(user.getName());
                    addMessage("Berhasil",true);
                    doRedirect("index.xhtml");
                }
                else{
                    addMessage("Password salah",false);
                }
            }
        }
    }
    
    public void verifyLogin(){
        if(session.getUsername()==null || session.getUsername().isEmpty())
            doRedirect("login.xhtml");
    }
    
    private void doRedirect(String url){
        try{
            facesContext.getExternalContext().redirect(url);
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    private void addMessage(String message,boolean cek) {
        String summary = message;
        if(cek == true){
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null));
        }else{
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, null));
        }
    }
    
    
}
