package com.daksa.tinyva.web;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.exception.InvalidUserException;
import com.daksa.tinyva.service.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class UserBacking implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Inject
    private UserService service;
    @Inject
    private FacesContext facesContext;
    
    private String usernames;
    private String password;
    private String name;
    private UserLogin user;
    private Boolean success;

    
    public String getUsernames() {
        return usernames;
    }

    public void setUsernames(String usernames) {
        this.usernames = usernames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLogin getUser() {
        return user;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void create(){
        
        Boolean check = true;
        if( usernames == null || usernames.isEmpty()){
            addMessage("Username tidak boleh kosong",false);
            check = false;
        }
        if ( password == null || password.isEmpty() ) {
            addMessage("Password tidak boleh kosong",false);
            check = false;
        }
        if ( name == null || name.isEmpty()) {
            addMessage("Nama tidak boleh kosong",false);
            check = false;
        }
        if (check == true) {
             try {
                user = new UserLogin();
                user.setUsername(usernames);
                user.setPassword(password);
                user.setName(name);
                service.create(user);
                success = true;
                addMessage("Berhasil",success);
                try {
                    facesContext.getExternalContext().redirect("login.xhtml");
                } catch (IOException ex) {
                    Logger.getLogger(UserBacking.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (InvalidUserException ex) {
                success = false;
                addMessage(ex.getMessage(),success);
            }
        }
    }
   
    private void addMessage(String message,boolean cek) {
        String summary = message;
        if(cek == true){
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null));
        }else{
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, null));
        }
    }
    
}
