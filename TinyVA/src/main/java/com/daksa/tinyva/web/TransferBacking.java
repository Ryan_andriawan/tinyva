/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tinyva.web;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.entity.TransferHistory;
import com.daksa.tinyva.exception.InsufficientBalanceException;
import com.daksa.tinyva.exception.InvalidTransferException;
import com.daksa.tinyva.model.TransferTotal;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.repository.TransferRepository;
import com.daksa.tinyva.service.AccountService;
import com.daksa.tinyva.service.TransferService;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.inject.Inject;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;



@Named
@ViewScoped
public class TransferBacking implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private TransferService service;
    
    @Inject
    private TransferRepository repo;
    
    @Inject
    private AccountRepository accRepo;
    
    @Inject
    private AccountService accountService;
    
    @Inject
    private FacesContext facesContext;
    
    private Account receiver;
    private Account sender;
    private String dest;
    private BigDecimal ammount;
    private LineChartModel lineModel;
    private LazyDataModel<TransferHistory> lazyTransferModel;
    private Boolean success;
    
    @PostConstruct
    public void init(){
        
        receiver = new Account();
        sender = new Account();
        ammount = BigDecimal.ZERO;
        lazyTransferModel = new LazyDataModel<TransferHistory>() {
            @Override
            public List<TransferHistory> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List<TransferHistory> data;
                Long count;
                data = repo.getLazyAllTransfer(first, pageSize, sortField, sortOrder, filters);
                count = repo.countTransfer(first, pageSize, sortField, sortOrder, filters);
                this.setRowCount(count.intValue());
                
                return data;

            }
        };
        createLineModels();
       
    }

    public LineChartModel getLineModel() {
        return lineModel;
    }
    
    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public Account getReceiver() {
        return receiver;
    }

    public void setReceiver(Account receiver) {
        this.receiver = receiver;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public Boolean getSuccess() {
        return success;
    }
    
    public LazyDataModel<TransferHistory> getLazyTransferModel() {
        return lazyTransferModel;
    }
    
    private void createLineModels() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        
        lineModel = initCategoryModel();
        
        lineModel.setTitle("Transfer History Chart");
        lineModel.setLegendPosition("e");
        lineModel.setAnimate(true);
        lineModel.getAxis(AxisType.Y).setLabel("Total Amount");
        DateAxis axis = new DateAxis("Dates");
        axis.setTickAngle(-50);
        axis.setMax(dateFormat.format(date));
        axis.setTickFormat("%b %#d, %y");

        lineModel.getAxes().put(AxisType.X, axis);
        
        
        
    }
    
    private LineChartModel initCategoryModel() {
        
        LineChartModel model = new LineChartModel();
        List<TransferTotal> transfers = repo.listTotalPerDate();
        
        ChartSeries acc = new ChartSeries();
        acc.setLabel("Account");
        if(!transfers.isEmpty()){

            for(Integer x = 0;x < transfers.size() ;x++){

                acc.set(transfers.get(x).getDate().toString(), transfers.get(x).getAmount());
            }
        }else{
            acc.set(0,0);
        }
        model.addSeries(acc);
        
           
        return model;
        
    }
    
    public void transfer() {
        if(dest == null || dest.isEmpty()){
            addMessage("Id penerima tidak boleh kosong",false);
        }
        else{
            if(ammount.compareTo(new BigDecimal(10000.00)) == -1){
                addMessage("Minimal transfer Rp 10.000,00",false);
            }
            else{
                try {
                    service.create(sender, receiver, ammount);
                    accountService.afterTransfer(sender, receiver);
                    success = true;
                    createLineModels();
                    addMessage("Berhasil", success);
                } catch (InsufficientBalanceException | InvalidTransferException ex) {
                    success = false;
                    addMessage(ex.getMessage(), success);
                }
                
            }   
        }
    }
    
    public void reset(Account acc){
                sender = acc;
                dest = null;
                receiver = null;
                ammount = BigDecimal.ZERO;
    }
    private void addMessage(String message,boolean cek) {
        String summary = message;
        if(cek == true){
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null));
        }else{
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, null));
        }
    }
    
    public void handleKeyEvent() {
        receiver  = accRepo.findAccount(dest);
    }
    
    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        facesContext.addMessage(null, msg);
    }
    
    
}
