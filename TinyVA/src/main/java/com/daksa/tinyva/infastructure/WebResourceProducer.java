package com.daksa.tinyva.infastructure;


import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;

/**
 *
 * @author DS-PRG-18
 */
@Dependent
public class WebResourceProducer implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Produces
    @RequestScoped
    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }
}
