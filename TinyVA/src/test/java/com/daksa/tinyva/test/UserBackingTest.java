package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.exception.InvalidUserException;
import com.daksa.tinyva.service.UserService;
import com.daksa.tinyva.web.UserBacking;
import java.util.concurrent.atomic.AtomicReference;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class UserBackingTest {
    
    @Mock
    private FacesContext facesContext;    
    @Mock 
    private UserService userService;    
    @InjectMocks
    private UserBacking userBacking;
    
    @Before
    public void before(){
        ExternalContext externalContext = mock(ExternalContext.class);
        when(facesContext.getExternalContext()).thenReturn(externalContext);
    }
    
    @Test
    public void testCreate_succsess() {
        
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        userBacking.setUsernames("test1");
        userBacking.setPassword("password1");
        userBacking.setName("name1");
        userBacking.create();
        
        assertEquals(FacesMessage.SEVERITY_INFO, facesMessageRef.get().getSeverity());
        assertEquals("Berhasil", facesMessageRef.get().getSummary());
    }
 
    
    @Test
    public void testCreate_InvalidUserException() throws Exception {
        doThrow(new InvalidUserException("Id Sudah Digunakan"))
                .when(userService).create(any(UserLogin.class));
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        userBacking.setUsernames("test1");
        userBacking.setPassword("password1");
        userBacking.setName("name1");
        userBacking.create();
        
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Id Sudah Digunakan", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testCreate_emptyField(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        userBacking.create();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
    }
    
}
