/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InvalidAccountException;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.service.AccountService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verify;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    
    @Mock
    private AccountRepository accountRepository;
    
    @Mock
    private EntityManager em;
    
    @InjectMocks
    private AccountService accountService;
    
    @Test
    public void testCreate_success() throws InvalidAccountException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        Account account = new  Account();
        account.setId("test1");
        account.setName("name1");
        account.setDob(date);
        account.setAllowNegative(Boolean.TRUE);
        account.setBalance(BigDecimal.ZERO);
        accountService.create(account);
        verify(em).persist(Mockito.any(Account.class));
    }
    
    @Test(expected = InvalidAccountException.class)
    public void testCreate_InvalidAccountException() throws InvalidAccountException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        Account account = new  Account();
        account.setId("test1");
        account.setName("name1");
        account.setDob(date);
        account.setAllowNegative(Boolean.TRUE);
        account.setBalance(BigDecimal.ZERO);
        Mockito.when(accountRepository.findAccount("test1")).thenReturn(account);
        accountService.create(account);
        
    }
    
    @Test
    public void testUpdate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        Account account = new  Account();
        account.setId("test1");
        account.setName("name1");
        account.setDob(date);
        account.setAllowNegative(Boolean.TRUE);
        account.setBalance(BigDecimal.ZERO);
        accountService.update(account);
        verify(em).merge(Mockito.any(Account.class));
    }
    
    @Test
    public void testAfterTransfer(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        Account sender = new  Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setAllowNegative(Boolean.TRUE);
        sender.setBalance(BigDecimal.ZERO);
        
        Account receiver = new  Account();
        receiver.setId("test1");
        receiver.setName("name1");
        receiver.setDob(date);
        receiver.setAllowNegative(Boolean.TRUE);
        receiver.setBalance(BigDecimal.ZERO);
        accountService.afterTransfer(sender, receiver);
        verify(em,Mockito.times(2)).merge(Mockito.any(Account.class));
    }
}
