package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InvalidAccountException;
import com.daksa.tinyva.service.AccountService;
import com.daksa.tinyva.web.AccountBacking;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountBackingTest {
    
    @Mock
    private AccountService accountService;
    
    @Mock
    private FacesContext facesContext;
    
    @InjectMocks
    private AccountBacking accountBacking;
    
    
    @Before
    public void before(){
        
    }
    
    @Test
    public void getIdTest(){
        accountBacking.setId("test1");
        Assert.assertEquals("test1", accountBacking.getId());
        
    }
    
    @Test
    public void getNameTest(){
        accountBacking.setName("name1");
        Assert.assertEquals("name1", accountBacking.getName());
    }
    
    @Test
    public void getDobTest(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        accountBacking.setDob(date);
        Assert.assertEquals(date,accountBacking.getDob());
    }
    
    @Test
    public void getAllowNegativeTest(){
        accountBacking.setAllowNegative(Boolean.TRUE);
        Assert.assertEquals(Boolean.TRUE, accountBacking.getAllowNegative());
    }
    
    @Test
    public void testCreate_InvalidAccountException() throws Exception{
        
        doThrow(new InvalidAccountException("Id Sudah Digunakan"))
                .when(accountService).create(any(Account.class));
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        accountBacking.setId("test1");
        accountBacking.setName("name1");
        accountBacking.setDob(date);
        accountBacking.setAllowNegative(Boolean.TRUE);
        accountBacking.create();
        
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Id Sudah Digunakan", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testCreate_emptyField(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        accountBacking.create();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        
    }
    
    @Test
    public void testCreate_Success() {
        
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        accountBacking.setId("test1");
        accountBacking.setName("name1");
        accountBacking.setDob(date);
        accountBacking.setAllowNegative(Boolean.TRUE);
        accountBacking.create();
        
        assertEquals(FacesMessage.SEVERITY_INFO, facesMessageRef.get().getSeverity());
        assertEquals("Berhasil", facesMessageRef.get().getSummary());
    }
}
