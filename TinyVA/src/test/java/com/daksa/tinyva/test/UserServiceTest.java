
package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.exception.InvalidUserException;
import com.daksa.tinyva.repository.UserRepository;
import com.daksa.tinyva.service.UserService;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    
    @Mock
    private UserRepository userRepository;
    
    @Mock
    private EntityManager entityManager;
    
    @InjectMocks
    private UserService userService;
    
    @Test
    public void testCreate_Success() throws InvalidUserException{
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        userService.create(user);
        Mockito.verify(entityManager).persist(Matchers.any(UserLogin.class));
    }
    
    @Test(expected = InvalidUserException.class)
    public void testCreate_InvalidUserException() throws InvalidUserException{
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1"); 
        
        Mockito.when(userRepository.findUser("username1")).thenReturn(user);
        userService.create(user);
    }
    
    @Test
    public void testUpdate(){
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        userService.update(user);
        Mockito.verify(entityManager).merge(Matchers.any(UserLogin.class));
    }
}
