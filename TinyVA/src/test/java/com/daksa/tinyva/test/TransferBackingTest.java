
package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.exception.InsufficientBalanceException;
import com.daksa.tinyva.exception.InvalidTransferException;
import com.daksa.tinyva.repository.AccountRepository;
import com.daksa.tinyva.repository.TransferRepository;
import com.daksa.tinyva.service.AccountService;
import com.daksa.tinyva.service.TransferService;
import com.daksa.tinyva.web.TransferBacking;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 *
 * @author DS-PRG-18
 */

@RunWith(MockitoJUnitRunner.class)
public class TransferBackingTest {
    
    @Mock
    private TransferService transferService;
    
    @Mock
    private TransferRepository transferRepository;
    
    @Mock
    private AccountRepository accountRepo;
    
    @Mock
    private AccountService accountService;
    
    @Mock
    private FacesContext facesContext;
    
    @InjectMocks
    private TransferBacking transferBacking;
    
    @Before
    public void before(){
        
    }
    
    @Test
    public void getDestTest(){
        transferBacking.setDest("test1");
        Assert.assertEquals("test1", transferBacking.getDest());
    }
    
    @Test
    public void getReceiverTest(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        transferBacking.setReceiver(receiver);
        Assert.assertEquals(receiver, transferBacking.getReceiver());
    }
    
    
    @Test
    public void getSenderTest(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test2");
        sender.setName("name2");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        transferBacking.setSender(sender);
        Assert.assertEquals(sender, transferBacking.getSender());
    }
    
    @Test
    public void testTransfer_InsufficientBalanceException() throws Exception{
        
        doThrow(new InsufficientBalanceException("Saldo tidak cukup"))
                .when(transferService).create(any(Account.class),any(Account.class),any(BigDecimal.class));
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        transferBacking.setDest("test2");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.FALSE);
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        BigDecimal amount = (new BigDecimal(250000.00));
        transferBacking.setSender(sender);
        transferBacking.setReceiver(receiver);
        transferBacking.setAmmount(amount);
        transferBacking.transfer();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Saldo tidak cukup", facesMessageRef.get().getSummary());
        
    }
    
    @Test
    public void testTransfer_InvalidTransferException() throws Exception{
        
        doThrow(new InvalidTransferException("Pengirim dan Penerima tidak boleh sama"))
                .when(transferService).create(any(Account.class),any(Account.class),any(BigDecimal.class));
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        transferBacking.setDest("test1");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        Account receiver = new Account();
        receiver.setId("test1");
        receiver.setName("name1");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        BigDecimal amount = (new BigDecimal(250000.00));
        transferBacking.setSender(sender);
        transferBacking.setReceiver(receiver);
        transferBacking.setAmmount(amount);
        transferBacking.transfer();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Pengirim dan Penerima tidak boleh sama", facesMessageRef.get().getSummary());
        
    }
    
    @Test
    public void testTransfer_success(){
        
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
         doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                transferBacking.getSender().setBalance(transferBacking.getSender().getBalance().subtract(transferBacking.getAmmount()));
                transferBacking.getReceiver().setBalance(transferBacking.getReceiver().getBalance().add(transferBacking.getAmmount()));
                return null;
            }
        }).when(accountService).afterTransfer(any(Account.class), any(Account.class));
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
         
        BigDecimal amount = (new BigDecimal(250000.00));
        
        transferBacking.setDest("test2");
        transferBacking.setSender(sender);
        transferBacking.setReceiver(receiver);
        transferBacking.setAmmount(amount);
        transferBacking.transfer();
         
        assertEquals(new BigDecimal(-250000), transferBacking.getSender().getBalance());
        assertEquals(new BigDecimal(250000), transferBacking.getReceiver().getBalance());
        assertEquals(FacesMessage.SEVERITY_INFO, facesMessageRef.get().getSeverity());
        assertEquals("Berhasil", facesMessageRef.get().getSummary());
    }
    
    
    @Test
    public void testTransfer_nullDestination(){
        
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        transferBacking.transfer();
        
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Id penerima tidak boleh kosong", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testTransfer_MinimumAmount(){
        
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        transferBacking.setDest("test2");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
         
        BigDecimal amount = (new BigDecimal(5000.00));
        transferBacking.setSender(sender);
        transferBacking.setReceiver(receiver);
        transferBacking.setAmmount(amount);
        transferBacking.transfer();
        
        Assert.assertTrue(transferBacking.getAmmount().compareTo(new BigDecimal(10000)) == -1 || transferBacking.getAmmount().compareTo(new BigDecimal(10000))== 0 );
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Minimal transfer Rp 10.000,00", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void handleKeyEventTest() {
        
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        transferBacking.setDest("test2");
        Mockito.when(accountRepo.findAccount("test2")).thenReturn(receiver);
        
        transferBacking.handleKeyEvent();
        Assert.assertEquals(receiver, transferBacking.getReceiver());
    }
    
}
