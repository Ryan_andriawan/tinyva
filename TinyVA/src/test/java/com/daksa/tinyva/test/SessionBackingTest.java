
package com.daksa.tinyva.test;

import com.daksa.tinyva.web.SessionBacking;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class SessionBackingTest {
    
    @Mock
    private FacesContext facesContext;
    
    @Mock
    private ExternalContext externalContext;
    
    @InjectMocks
    private SessionBacking sessionBacking;
    
    @Test
    public void logout(){
        
        Mockito.when(facesContext.getExternalContext()).thenReturn(externalContext);
        Assert.assertEquals("/login.xhtml?faces-redirect=true",sessionBacking.logout());
    }
}
