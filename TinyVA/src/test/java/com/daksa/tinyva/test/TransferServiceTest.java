/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.Account;
import com.daksa.tinyva.entity.TransferHistory;
import com.daksa.tinyva.exception.InsufficientBalanceException;
import com.daksa.tinyva.exception.InvalidTransferException;
import com.daksa.tinyva.repository.TransferRepository;
import com.daksa.tinyva.service.AccountService;
import com.daksa.tinyva.service.TransferService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.EntityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {
    
    @Mock
    private TransferRepository transferRepository;
    
    @Mock
    private AccountService accountService;
    
    @Mock
    private EntityManager em;
    
    @InjectMocks
    private TransferService transferService;
    
    @Test(expected = InvalidTransferException.class)
    public void testCreate_InvalidTransferException() throws InsufficientBalanceException, InvalidTransferException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        Account receiver = new Account();
        receiver.setId("test1");
        receiver.setName("name1");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        BigDecimal amount = (new BigDecimal(250000.00));
        Mockito.when(transferRepository.getLastID(2016)).thenReturn("trf2016001");
        transferService.create(sender, receiver, amount);
        
    }
    
    @Test(expected = InsufficientBalanceException.class)
    public void testCreate_InsufficientBalanceException() throws InsufficientBalanceException, InvalidTransferException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.FALSE);
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        BigDecimal amount = (new BigDecimal(250000.00));
        Mockito.when(transferRepository.getLastID(2016)).thenReturn("trf2016001");
        transferService.create(sender, receiver, amount);
        
    }
    
    @Test
    public void testCreate_Success() throws InsufficientBalanceException, InvalidTransferException{
        
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sdf.parse("1994-05-05");
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        
        Account sender = new Account();
        sender.setId("test1");
        sender.setName("name1");
        sender.setDob(date);
        sender.setBalance(BigDecimal.ZERO);
        sender.setAllowNegative(Boolean.TRUE);
        
        Account receiver = new Account();
        receiver.setId("test2");
        receiver.setName("name2");
        receiver.setDob(date);
        receiver.setBalance(BigDecimal.ZERO);
        receiver.setAllowNegative(Boolean.TRUE);
        
        BigDecimal amount = (new BigDecimal(250000.00));
        Mockito.when(transferRepository.getLastID(2016)).thenReturn("trf2016001");
        transferService.create(sender, receiver, amount);
        
        verify(em).persist(Mockito.any(TransferHistory.class));
    }
    
}
