package com.daksa.tinyva.test;

import com.daksa.tinyva.entity.UserLogin;
import com.daksa.tinyva.repository.UserRepository;
import com.daksa.tinyva.service.UserService;
import com.daksa.tinyva.web.LoginBacking;
import com.daksa.tinyva.web.SessionBacking;
import java.util.concurrent.atomic.AtomicReference;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doAnswer;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

/**
 *
 * @author DS-PRG-18
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginBackingTest {
    
    @Mock
    private UserService userService;
    
    @Mock
    private SessionBacking sessionBacking;
    
    @Mock
    private FacesContext facesContext;
    
    @Mock
    private ExternalContext externalContext;
    
    @Mock
    private UserRepository userRepository;
    
    @InjectMocks
    private LoginBacking loginBacking;
    
    @Before
    public void before(){
        
    }
    
    @Test
    public void testLogin_success(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setUsername("username1");
        loginBacking.setPassword("password1");
        
        Mockito.when(userRepository.findUser("username1")).thenReturn(user);
        Mockito.when(facesContext.getExternalContext()).thenReturn(externalContext);
     
        loginBacking.login();
        
        assertEquals(FacesMessage.SEVERITY_INFO, facesMessageRef.get().getSeverity());
        assertEquals("Berhasil", facesMessageRef.get().getSummary());
        
    }
    
    @Test
    public void testLogin_nullUser(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setUsername("username1");
        loginBacking.setPassword("password1");
        
        Mockito.when(userRepository.findUser("username1")).thenReturn(null);
     
        loginBacking.login();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Username tidak terdaftar", facesMessageRef.get().getSummary());
        
    }
    
    @Test
    public void testLogin_IncorectPassword(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setUsername("username1");
        loginBacking.setPassword("password2");
        
        Mockito.when(userRepository.findUser("username1")).thenReturn(user);
     
        loginBacking.login();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Password salah", facesMessageRef.get().getSummary());
        
    }
    
     @Test
    public void testLogin_EmptyField(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        loginBacking.login();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        
    }
    
    
    
    @Test
    public void testUpdate_success(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setName("name2");
        loginBacking.setOldPassword("password1");
        loginBacking.setNewPassword("newpass1");
        loginBacking.setConfirmPassword("newpass1");
        
        
        Mockito.when(userRepository.findUser( sessionBacking.getUsername())).thenReturn(user);
        loginBacking.update();
        assertEquals("name2", loginBacking.getUser().getName());
        assertEquals("newpass1", loginBacking.getUser().getPassword());
        assertEquals(FacesMessage.SEVERITY_INFO, facesMessageRef.get().getSeverity());
        assertEquals("Updated", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testUpdate_EmptyNewPassword(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setName("name1");
        loginBacking.setOldPassword("password1");
        loginBacking.setNewPassword("");
        loginBacking.setConfirmPassword("");
        
        
        Mockito.when(userRepository.findUser( sessionBacking.getUsername())).thenReturn(user);
        loginBacking.update();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Password baru tidak boleh kosong", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testUpdate_WrongOldPassword(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setName("name1");
        loginBacking.setOldPassword("password2");
        loginBacking.setNewPassword("newpass1");
        loginBacking.setConfirmPassword("newpass1");
        
        
        Mockito.when(userRepository.findUser( sessionBacking.getUsername())).thenReturn(user);
        loginBacking.update();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Password salah", facesMessageRef.get().getSummary());
    }
    
    @Test
    public void testUpdate_InvalidConfirmPassword(){
        final AtomicReference<FacesMessage> facesMessageRef = new AtomicReference<>();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                FacesMessage facesMessage = (FacesMessage) invocation.getArguments()[1];
                facesMessageRef.set(facesMessage);
                return null;
            }
        }).when(facesContext).addMessage(any(String.class), any(FacesMessage.class));
        
        
        UserLogin user = new UserLogin();
        user.setUsername("username1");
        user.setPassword("password1");
        user.setName("name1");
        
        loginBacking.setName("name1");
        loginBacking.setOldPassword("password1");
        loginBacking.setNewPassword("newpass1");
        loginBacking.setConfirmPassword("newpass2");
        
        
        Mockito.when(userRepository.findUser( sessionBacking.getUsername())).thenReturn(user);
        loginBacking.update();
        assertEquals(FacesMessage.SEVERITY_WARN, facesMessageRef.get().getSeverity());
        assertEquals("Konfirmasi password tidak sama", facesMessageRef.get().getSummary());
    }
    
    
}
